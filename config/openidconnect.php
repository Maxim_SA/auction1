<?php

return [
    "client_id" => env("CLIENT_ID"),
    "client_secret" => env("CLIENT_SECRET"),
    "token_route" => "http://sso_auth.local/token/get-access-token",
//    "token_route" => "http://oauth2_demo.local/lockdin/token",
//    "authorize_route" => "http://oauth2_demo.local/lockdin/authorize",
    "authorize_route" => "http://sso_auth.local/authorize",
//    "resource_route" => "http://oauth2_demo.local/lockdin/resource",
    "resource_route" => "http://sso_auth.local/resource/get-friends",
    "resource_method" => "POST",
    "resource_params" => ["debug" => true],
    "user_credentials" => ["demouser", "testpass"],
    "http_options" => ["exceptions" > false],
];