# auction1

Auction1 - it's a client for sso


Installation
------------

Use [Composer](http://getcomposer.org/) to install this application:

    $ git clone https://Maxim_SA@bitbucket.org/Maxim_SA/auction1.git
    $ cd auction1
    $ curl -s http://getcomposer.org/installer | php
    $ ./composer.phar install
    $ npm i 
    $ npm run build:dev
    $ cp .env.example .env
    $ php artisan key:generate

Add to the .env file

    CLIENT_ID=demoapp
    CLIENT_SECRET=demopass
    
Setup Apache virtual host
```
<VirtualHost *:80>
        ServerName auction1.local
        ServerAdmin webmaster@localhost
        DocumentRoot /Path/to/the/folder/auction1/wwwroot
        ServerAlias *.auction1.local
        <Directory "/Projects/www/sam/sso/auction1/wwwroot">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
        </Directory>

ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory "/usr/lib/cgi-bin">
    AllowOverride All
    Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    Require all granted
</Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
``````

