/**
 * Created by namax on 02/03/17.
 */
const path = require('path');
const webpack = require('webpack');
const rootDir = path.resolve(__dirname, '');
const entryPoint = './resources/assets/js';
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  context: rootDir,
  entry: {
    //Entry points of applications.
    // Admin page vendors libs
    main: entryPoint + '/app.js',
  },
  output: {
    path: path.resolve(rootDir, 'wwwroot/dist'),
    publicPath: '/wwwroot/dist/',
    filename: '[name].js',
    chunkFilename: '[chunkhash].js',
  },
  resolve: {
    extensions: ['.js', '.json'],
    modules: [
      path.resolve(rootDir, 'node_modules'),
      path.resolve(rootDir, 'resources/assets/js'),
      path.resolve(rootDir, 'resources/assets/sass'),
    ],
  },
  module: {
    rules: [
      {
        test: /\.css$/, use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {},
          },
          'css-loader',
        ]
        ,
      },
      {test: /\.(png|jpg|gif)$/, loader: 'url-loader'},
      {
        // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {},
          },
          {
            loader: 'css-loader',
            options: {
              url: false,
              sourceMap: true,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              presets: ['@babel/preset-env'],
              plugins: [
                [
                  '@babel/plugin-transform-runtime', {
                  'corejs': 3,
                }],
                'syntax-async-functions',
                'syntax-dynamic-import',
                '@babel/plugin-transform-async-to-generator',
                '@babel/plugin-transform-regenerator',
                '@babel/plugin-proposal-class-properties',
              ],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      allChunks: true,
    }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'async',
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      name: true,
      cacheGroups: {
        default: {
          minChunks: 2,
          priority: 0,
          reuseExistingChunk: true,
        },
        vendors: {
          test: /node_modules/,
          priority: 0,
        },
      },
    },
  },
};