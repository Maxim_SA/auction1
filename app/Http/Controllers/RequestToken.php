<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RequestToken extends Controller
{

//    public static function addRoutes($routing)
//    {
//        $routing->get('/client/request_token/authorization_code', array(new self(), 'requestTokenWithAuthCode'))->bind('request_token_with_authcode');
//        $routing->get('/client/request_token/user_credentials', array(new self(), 'requestTokenWithUserCredentials'))->bind('request_token_with_usercredentials');
//        $routing->get('/client/request_token/refresh_token', array(new self(), 'requestTokenWithRefreshToken'))->bind('request_token_with_refresh_token');
//    }

    public function autorizationCode(Request $request)
    {
        $http = new Client();  // service to make HTTP requests to the oauth server

        $code = $request->get('code');

        $redirect_uri_params = array_filter([
            'show_refresh_token' => $request->get('show_refresh_token'),
        ]);

        // exchange authorization code for access token
        $body = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => config('openidconnect.client_id'),
            'client_secret' => config('openidconnect.client_secret'),
            'redirect_uri' => urlencode(route('ReceiveAuthorizationCode', $redirect_uri_params)),
        ];

        // determine the token endpoint to call based on our config
        $endpoint = config('openidconnect.token_route');

        // make the token request via http and decode the json response
        $response = $http->post(
            $endpoint,
            [
                "form_params" => $body,
                'http_errors' => false
            ]
        );
        $json = json_decode((string)$response->getBody(), true);

        // if it is successful, display the token in our app
        if (isset($json['access_token'])) {
            if ($request->get('show_refresh_token')) {
                return view('request-token.show_refresh_token', ['response' => $json]);
            }

            return view('request-token.show_access_token', ['response' => $json]);
        }

        return view('request-token.failed_token_request', ['response' => $json ? $json : $response]);
    }


}
