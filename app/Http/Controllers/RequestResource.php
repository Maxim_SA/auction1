<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RequestResource extends Controller
{
//    public static function addRoutes($routing)
//    {
//        $routing->get('/client/request_resource', array(new self(), 'requestResource'))->bind('request_resource');
//    }

    public function requestResource(Request $request)
    {

        $http = new Client();  // service to make HTTP requests to the oauth server

        // pull the token from the request
        $token = $request->get('token');

        // determine the resource endpoint to call based on our config (do this somewhere else?)
        $apiRoute = config('openidconnect.resource_route');
        $endpoint = 0 === strpos($apiRoute, 'http') ? $apiRoute : route('$apiRoute');

        // make the resource request and decode the json response
        $response = $http->get($endpoint, ['query' => ['access_token' => $token]]);
        $json = json_decode((string)$response->getBody(), true);

        return view(
            'request-resource/show_resource',
            [
                'response' => $json ? $json : $response,
                'resource_uri' => $request->getBaseUrl()
            ]
        );
    }
}
