<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 10/31/18
 * Time: 11:18 AM
 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceiveAuthorizationCode extends Controller
{
    public function index(Request $request)
    {

        if (!$code = $request->get('code')) {
            return view('receive-authorization-code.failed_authorization', ['response' => "no code"]);
        }

        // verify the "state" parameter matches this user's session (this is like CSRF - very important!!)
        if ($request->get('state') !== $request->session()->token()) {
            return view('receive-authorization-code.failed_authorization', ['response' => ['error_description' => 'Your session has expired.  Please try again.']]);
        }
        $rawIdTocken = $request->get('id_token');
        $showRefreshToken = $request->get('show_refresh_token', false);

        $rawIdTockenParts = explode(".", $rawIdTocken);
        $idTocken = [];
        foreach ($rawIdTockenParts as $rawIdTockenPart) {
            $idTocken[] = base64_decode($rawIdTockenPart);
        }


        return view('receive-authorization-code.show_authorization_code', [
            'code' => $code,
            'id_token' => $rawIdTocken,
            'id_token_decoded' => $idTocken,
            'show_refresh_token' => $showRefreshToken,
        ]);
    }

}