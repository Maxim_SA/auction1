<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 8/9/18
 * Time: 10:10 AM
 */

namespace App\Http\Controllers;


use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('home.index', []);
    }

    public function openidconnect(Request $request)
    {
        return view('home.openidconnect', [
            'sessionId' => $request->session()->token()
        ]);
    }
}