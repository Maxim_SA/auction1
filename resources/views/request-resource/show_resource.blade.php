@extends('layouts.layout')

@section('title', 'Authorization Failed!')


@section('content')
    <div class="container">


    <h3>Resource Request Complete!</h3>

    <p>Here is the full JSON response: </p>

    {{ var_dump($response) }}

    <p>
        You have successfully called the APIs with your Access Token.  Here are your friends:
    </p>
    <ul>
        @foreach ($response['friends'] as $friend)
            <li>{{ $friend }}</li>
        @endforeach
    </ul>

    <pre><code>  The API call can be seen at <a href="{{ $resource_uri }}" target="_blank">{{$resource_uri}}</a></code></pre>


    </div>

@endsection
