@extends('layouts.layout')

@section('title', 'Profile')


@section('content')
    {{--{{ config('app.name') }}--}}
    {{--{{ config('openidconnect.client_id') }}--}}
    {{--{{ config('openidconnect.authorize_route') }}--}}
    {{--{{ config('openidconnect.authorize_redirect') }}--}}
    {{--{{str_random()}}--}}


    <a class="button"
       href="{{ config('openidconnect.authorize_route') }}?response_type=code%20id_token&client_id={{ config('openidconnect.client_id') }}&redirect_uri={{urlencode(route('ReceiveAuthorizationCode'))}}&scope=openid&state={{$sessionId}}&nonce={{str_random()}}">Authorization
        Code + ID Token</a>


    {{--<div class="container">--}}
    {{--<p>Auth client test.</p>--}}

    {{--<p>--}}
    {{--<a class="button" href="{{ app.parameters.authorize_route|slice(0, 4) == 'http' ? app.parameters.authorize_route : url(app.parameters.authorize_route) }}?response_type=code%20id_token&client_id={{ config('openidconnect.client_id') }}&redirect_uri={{ url('authorize_redirect')|url_encode() }}&scope=openid&state={{session_id}}&nonce={{random()}}">Authorization Code + ID Token</a>--}}
    {{--<div class="help">--}}
    {{--Same as above, but with the "code id_token" response type. The ID Token comes back with the Authorization Code.--}}
    {{--</div>--}}
    {{--</p>--}}
    {{--</div>--}}
@endsection