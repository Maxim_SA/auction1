@extends('layouts.layout')

@section('title', 'Authorization Failed!')


@section('content')
    <div class="container">

        <h3>Authorization Code Retrieved!</h3>
        <p>We have been given an <strong>Authorization Code</strong> from the OAuth2.0 Server:</p>
        <pre><code>  Authorization Code: {{ $code }}  </code></pre>

        <p>
            Now exchange the Authorization Code for an <strong>Access Token</strong>:
        <p>
            {{--href="/client/request_token/authorization_code?code=ed498914a2a5c3c52a7889af8fe0478d838dd043"--}}
            <a class="button"
               href="{{ route('RequestTokenWithAuthorizationCode', ['code'=> $code,  "show_refresh_token" => $show_refresh_token]) }}">make
                a token request</a>

        <div class="help">
            <em>usually this is done behind the scenes, but we're going step-by-step so you don't miss anything!</em>
        </div>

        <h3>Open ID Connect ID Token</h3>
        <p>
            Because you requested the token using <code>scope=openid</code>, you have also received an
            ID token!
        </p>

        <pre><code>  ID Token: {{ $id_token }}  </code></pre>


        {{ var_dump($id_token_decoded) }}


        {{--<a href="{{ path('homepage') }}">back</a>--}}
    </div>


@endsection
