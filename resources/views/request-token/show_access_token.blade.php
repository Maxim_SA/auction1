@extends('layouts.layout')

@section('title', 'Authorization Failed!')


@section('content')
    <div class="container">
        <h3>Token Retrieved!</h3>
        <pre><code>  Access Token: {{ $response["access_token"] }}  </code></pre>

        <div class="help"><em>Expires in {{ $response["expires_in"] }} seconds</em></div>

        <p>
            Now use this token to make a request to the OAuth2.0 Server's APIs:
        </p>

        {{--        <a class="button" href="{{ path('request_resource', { 'token': response.access_token }) }}">make a resource request</a>--}}

        <a class="button"
           href="{{ route('RequestResource', ['token'=> $response['access_token'] ]) }}">make
            a resource request</a>

        <div class="help"><em>This token can now be used multiple times to make API requests for this user.</em></div>

        {{ var_dump($response) }}


        {{--<a href="{{ path('homepage') }}">back</a>--}}
    </div>


@endsection
