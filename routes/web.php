<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/home/openidconnect', 'HomeController@openidconnect')->name("openidconnect");
Route::get('/receive-authorization-code', 'ReceiveAuthorizationCode@index')->name("ReceiveAuthorizationCode");
Route::get('/request-token/authorization-code', 'RequestToken@autorizationCode')->name("RequestTokenWithAuthorizationCode");
Route::get('/request-resource/authorization-code', 'RequestResource@requestResource')->name("RequestResource");
//Route::get('/profile/{id}', 'HomeController@profile');